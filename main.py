from flask import Flask, jsonify, request
from datetime import datetime
from pytz import timezone
from urllib.parse import urlparse
import socket
import ipapi

app = Flask(__name__)
app.debug = True



@app.route("/", methods=['POST', 'GET'])
def hello_world():
    format = "%Y-%m-%d %H:%M:%S %Z%z"
    dict  = {}
    IST = timezone('Asia/Kolkata')
    datetime_ist = datetime.now(IST)
    dict['Current Time'] = datetime_ist.strftime('%Y:%m:%d %H:%M:%S %Z %z')
    host_name=socket.gethostname()
    dict['Hostname'] = host_name
    dict['IP'] = socket.gethostbyname(host_name)
    search = request.form.get('search')
    geodata = ipapi.location(ip=search, output='json')
    dict['City'] = geodata['city']
    dict['Region'] = geodata['region']
    dict['Country'] = geodata['country']
    return jsonify(dict)


if __name__== '__main__':
    app.run(host='0.0.0.0', port=4545) 